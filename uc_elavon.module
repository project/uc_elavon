<?php

/**
 * @file
 * Implements Elavon payment services for use in Drupal Ubercart.
 */

// Elavon transaction mode definitions:
define('UC_ELAVON_TXN_MODE_PRODUCTION', 'production');
define('UC_ELAVON_TXN_MODE_TEST', 'test');

/**
 * Implements hook_uc_payment_gateway().
 */
function uc_elavon_uc_payment_gateway() {
  $gateways['elavon'] = array(
    'title' => t('Elavon direct processing'),
    'description' => t('Integrates Elavon direct processing payment, direct or delayed capture'),
    'settings' => 'uc_elavon_settings_form',
    'credit' => 'uc_elavon_charge',
    'credit_txn_types' => array(UC_CREDIT_AUTH_CAPTURE),
  );

  return $gateways;
}

/**
 * Implements hook_settings_form().
 */
function uc_elavon_settings_form($form, &$form_state) {
  $form = array();
  $form['uc_elavon_accountid'] = array(
    '#type' => 'textfield',
    '#title' => t('Elavon Account ID'),
    '#description' => t('Your Elavon Account ID'),
    '#default_value' => variable_get('uc_elavon_accountid'),
    '#required' => TRUE,
    '#weight' => -5,
  );

  $form['uc_elavon_userid'] = array(
    '#type' => 'textfield',
    '#title' => t('Elavon user ID'),
    '#description' => t('Your Elavon user id dedicated for web based transaction.'),
    '#default_value' => variable_get('uc_elavon_userid'),
    '#required' => TRUE,
    '#weight' => -4,
  );

  $form['uc_elavon_pin'] = array(
    '#type' => 'textfield',
    '#title' => t('Elavon Pin'),
    '#description' => t('Your Elavon Pin'),
    '#default_value' => variable_get('uc_elavon_pin'),
    '#required' => TRUE,
    '#weight' => -3,
  );

  $form['uc_elavon_txn_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Transaction mode'),
    '#description' => t('Adjust to live transactions when you are ready to start processing real payments.'),
    '#options' => array(
      UC_ELAVON_TXN_MODE_PRODUCTION => t('Live transactions in a production account'),
      UC_ELAVON_TXN_MODE_TEST => t('Test transactions with your account'),
    ),
    '#default_value' => variable_get('uc_elavon_txn_mode'),
  );

  $form['uc_elavon_log'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Log the following messages for debugging. These messages are credit and transaction information. Keep it off for production site.'),
    '#options' => array(
      'request' => t('API request messages'),
      'response' => t('API response messages'),
    ),
    '#default_value' => variable_get('uc_elavon_log'),
  );

  return $form;
}

/**
 * Main handler for processing credit card transactions.
 */
function uc_elavon_charge($order_id, $amount, $data) {
  module_load_include('inc', 'uc_elavon', 'uc_elavon.transaction');
  
  // Load the order.
  $order = uc_order_load($order_id);

  // Perform the appropriate action based on the transaction type.
  return _uc_elavon_charge($order, $amount, $data);
}
