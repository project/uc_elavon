<?php

/**
 * @file
 * Hooks provided by the Ubercart Elavon module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Allows transaction data to be altered before sending to Elavon.
 */
function hook_uc_elavon_request_alter(&$data) {
  $data['ssl_show_form'] = FALSE;
}

/**
 * @} End of "addtogroup hooks".
 */
