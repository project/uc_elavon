<?php

/**
 * @file
 * Callback for Elavon direct processing Transaction.
 */

/**
 * Handles authorizations and captures through Elavon direct processing.
 */
function _uc_elavon_charge($order, $amount, $data) {
  $billing_data = uc_elavon_get_billing_info($order);
  $info = array(
    'ssl_card_number' => check_plain($order->payment_details['cc_number']),
    'ssl_exp_date' => str_pad(check_plain($order->payment_details['cc_exp_month']), 2, '0', STR_PAD_LEFT) . substr(check_plain($order->payment_details['cc_exp_year']), 2, 2),
  );
  if (isset($order->payment_details['cc_cvv'])) {
    $info['CVNum'] = check_plain($order->payment_details['cc_cvv']);
  }

  // Build a name-value pair array for this transaction.
  $info += array(
    'ssl_amount' => uc_currency_format(check_plain($amount), FALSE, FALSE, '.'),
    'MagData' => '',
  );

  // Set Tax amount.
  $sales_tax_total = 0;

  $info += array(
    'ssl_avs_address' => check_plain($billing_data['street']),
    'ssl_avs_zip' => check_plain($billing_data['zip']),
    'ssl_first_name' => check_plain($billing_data['first_name']),
    'ssl_last_name' => check_plain($billing_data['last_name']),
    'ssl_transaction_type' => 'ccsale',
    'ssl_cvv2cvc2' => check_plain($order->payment_details['cc_cvv']),
    'ssl_salestax' => $sales_tax_total,
  );

  // Add additional transaction information to the request array.
  $info += array(
    'InvNum' => check_plain($order->order_id),
  );

  $info += array(
    'NameOnCard' => check_plain($billing_data['name_on_card']),
    'Street' => check_plain($billing_data['street']),
    'Zip' => check_plain($billing_data['zip']),
    'ExtData' => check_plain($billing_data['ext_data']),
  );

  return uc_elavon_transaction_process($order, $amount, $data, $info);
}

/**
 * Prepare ExtData XML element.
 */
function uc_elavon_get_billing_info($order) {
  $billing_data = array(
    'ext_data' => '',
    'street' => '',
    'street2' => '',
    'city' => '',
    'state' => '',
    'zip' => '',
    'country' => '',
    'name_on_card' => '',
    'first_name' => '',
    'last_name' => '',
  );

  $order_wrapper = entity_metadata_wrapper('uc_order', $order);

  if ($order_wrapper->billing_address->value()) {
    $billing_address = $order_wrapper->billing_address->value();
    $delivery_address = $order_wrapper->delivery_address->value();

    $ext_data = '';

    $billing_full_address = check_plain($billing_address->street1);
    if (isset($billing_address->street2) && !empty($billing_address->street2)) {
      $billing_full_address .= ', ' . check_plain($billing_address->street2);
    }

    $delivery_full_address = check_plain($delivery_address->street1);
    if (isset($delivery_address->street2) && !empty($delivery_address->street2)) {
      $delivery_full_address .= ', ' . check_plain($delivery_address->street2);
    }

    $billing_data['first_name'] = check_plain($billing_address->first_name);
    $billing_data['last_name'] = check_plain($billing_address->last_name);

    $country = uc_get_country_data(array('country_id' => check_plain($billing_address->country)));
    $country = reset($country);

    // Build and populate the API request SimpleXML element.
    $ext_data .= '<CustomerID>' . substr(check_plain($order->uid), 0, 20) . '</CustomerID>';

    // Customer Billing Address.
    $ext_data .= '<Invoice><BillTo>';
    $name_on_card = substr(check_plain($billing_data['first_name']), 0, 50) . ' ' . substr(check_plain($billing_data['last_name']), 0, 50);

    // Use company name as billing name when available.
    if (!empty($billing_address->company)) {
      $ext_data .= '<Name>' . substr(check_plain($billing_address->company), 0, 50) . '</Name>';

    }
    else {

      $ext_data .= '<Name>' . $name_on_card . '</Name>';
    }

    $ext_data .= '<Email>' . substr(check_plain($order->primary_email), 0, 255) . '</Email>';
    $ext_data .= '<Address>';
    $ext_data .= '<Street>' . substr($billing_full_address, 0, 60) . '</Street>';
    $ext_data .= '<City>' . substr(check_plain($billing_address->city), 0, 40) . '</City>';
    $ext_data .= '<State>' . substr(uc_get_zone_code(check_plain($billing_address->zone)), 0, 40) . '</State>';
    $ext_data .= '<Zip>' . substr(check_plain($billing_address->postal_code), 0, 20) . '</Zip>';
    $ext_data .= '<Country>' . $country['country_name'] . '</Country>';
    $ext_data .= '</Address>';
    $ext_data .= '</BillTo></Invoice>';

    $ext_data = format_string($ext_data);
    
    $billing_data['ext_data'] = $ext_data;
    $billing_data['street'] = substr(check_plain($billing_address->street1), 0, 30);
    $billing_data['street2'] = substr(check_plain($billing_address->street2), 0, 30);
    $billing_data['city'] = substr(check_plain($billing_address->city), 0, 30);
    $billing_data['state'] = substr(uc_get_zone_code(check_plain($billing_address->zone)), 0, 30);
    $billing_data['zip'] = substr(check_plain($billing_address->postal_code), 0, 9);
    $billing_data['country'] = uc_get_country_data(array('country_id' => check_plain($billing_address->country)));
    $billing_data['name_on_card'] = $name_on_card;
  }

  return $billing_data;
}

/**
 * Handles authorizations and captures through Elavon.
 */
function uc_elavon_transaction_process($order, $amount, $data, $info) {
  global $user;
  $response = uc_elavon_request($amount, $data, $info);
  if ($response['status']) {

    $result = array(
      'success' => TRUE,
      'comment' => isset($response['payment_log_message']) ? $response['payment_log_message'] : '',
      'data' => array(
        'module' => 'uc_elavon',
        'txn_type' => _uc_elavon_txn_type($data['txn_type']),
        'txn_id' => isset($response['ssl_txn_id']) ? $response['ssl_txn_id'] : '',
        'txn_authcode' => $data['txn_type'],
      ),
      'uid' => $user->uid,
    );

  }
  else {

    $result = array(
      'success' => FALSE,
      'message' => $response['msg'],
      'uid' => $user->uid,
    );

    drupal_set_message($response['msg'], 'error');
  }

  if (in_array($data['txn_type'], array(UC_CREDIT_AUTH_ONLY))) {
    $result['log_payment'] = FALSE;
  }
  else {

    $result['log_payment'] = TRUE;
  }

  // Save the comment to the order.
  uc_order_comment_save($order->order_id, $user->uid, $response['msg'], 'admin');

  return $result;
}

/**
 * Submits a request to Elavon.
 */
function uc_elavon_request($amount, $data, $info = array()) {
  // Get the API endpoint URL for the method's transaction mode and type.
  $url = uc_elavon_server_url();

  // Add the default name-value pairs to the array.
  $info += array(
    'ssl_merchant_id' => trim(variable_get('uc_elavon_accountid')),
    'ssl_user_id' => trim(variable_get('uc_elavon_userid')),
    'ssl_pin' => trim(variable_get('uc_elavon_pin')),
    'ssl_show_form' => 'false',
  );

  // Allow modules to alter parameters of the API request.
  drupal_alter('uc_elavon_request', $info);

  $elavon_log = trim(variable_get('uc_elavon_log'));

  // Log the request if specified.
  if ($elavon_log['request'] == 'request') {
    // Mask the credit card number and CVV.
    $log_nvp = $info;
    $log_nvp['ssl_merchant_id'] = str_repeat('X', strlen($log_nvp['ssl_merchant_id']));
    $log_nvp['ssl_user_id'] = str_repeat('X', strlen($log_nvp['ssl_user_id']));
    $log_nvp['ssl_pin'] = str_repeat('X', strlen($log_nvp['ssl_pin']));

    if (!empty($log_nvp['ssl_card_number'])) {
      $log_nvp['ssl_card_number'] = str_repeat('X', strlen($log_nvp['ssl_card_number']) - 4) . substr($log_nvp['ssl_card_number'], -4);
    }

    if (!empty($log_nvp['CcAccountNum'])) {
      $log_nvp['CcAccountNum'] = str_repeat('X', strlen($log_nvp['CcAccountNum']) - 4) . substr($log_nvp['CcAccountNum'], -4);
    }

    if (!empty($log_nvp['CVNum'])) {
      $log_nvp['CVNum'] = str_repeat('X', strlen($log_nvp['CVNum']));
    }

    watchdog('uc_elavon', 'Elavon request to @url: !param', array(
      '@url' => $url,
      '!param' => '<pre>' . check_plain(print_r($log_nvp, TRUE)) . '</pre>',
    ), WATCHDOG_DEBUG);
  }

  // Prepare xml for Elavon.
  $xmldata = "xmldata=<txn>";
  $xmldata .= "<ssl_merchant_id>" . check_plain($info['ssl_merchant_id']) . "</ssl_merchant_id>";
  $xmldata .= "<ssl_user_id>" . check_plain($info['ssl_user_id']) . "</ssl_user_id>";
  $xmldata .= "<ssl_pin>" . check_plain($info['ssl_pin']) . "</ssl_pin>";
  $xmldata .= "<ssl_show_form>" . check_plain($info['ssl_show_form']) . "</ssl_show_form>";
  $xmldata .= "<ssl_test_mode>false</ssl_test_mode>";
  $xmldata .= "<ssl_transaction_type>ccsale</ssl_transaction_type>";
  $xmldata .= "<ssl_cvv2cvc2>" . check_plain($info['CVNum']) . "</ssl_cvv2cvc2>";
  $xmldata .= "<ssl_card_number>" . check_plain($info['ssl_card_number']) . "</ssl_card_number>";
  $xmldata .= "<ssl_exp_date>" . check_plain($info['ssl_exp_date']) . "</ssl_exp_date>";
  $xmldata .= "<ssl_invoice_number>" . check_plain($info['InvNum']) . "</ssl_invoice_number>";
  $xmldata .= "<ssl_amount>" . check_plain($info['ssl_amount']) . "</ssl_amount>";
  $xmldata .= "<ssl_avs_address>" . check_plain($info['ssl_avs_address']) . "</ssl_avs_address>";
  $xmldata .= "<ssl_avs_zip>" . check_plain($info['ssl_avs_zip']) . "</ssl_avs_zip>";
  $xmldata .= "<ssl_first_name>" . check_plain($info['ssl_first_name']) . "</ssl_first_name>";
  $xmldata .= "<ssl_last_name>" . check_plain($info['ssl_last_name']) . "</ssl_last_name>";
  $xmldata .= "<ssl_salestax>" . check_plain($info['ssl_salestax']) . "</ssl_salestax></txn>";
    
  $xmldata = format_string($xmldata);
  
  // Setup the cURL request.
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_VERBOSE, 0);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, "$xmldata");
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
  curl_setopt($ch, CURLOPT_NOPROGRESS, 1);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
  curl_setopt($ch, CURLOPT_SSLVERSION, 6);
  $result = curl_exec($ch);

  // Log any errors to the watchdog.
  if ($error = curl_error($ch)) {
    watchdog('uc_elavon', 'cURL error: @error', array('@error' => $error), WATCHDOG_ERROR);
    $response['status'] = FALSE;
    $response['msg'] = $error;
    return $response;
  }
  curl_close($ch);

  // If we received data back from the server.
  if (empty($result)) {
    watchdog('uc_elavon', 'cURL error empty result returned.', array(), WATCHDOG_ERROR);
    $response['status'] = FALSE;
    $response['msg'] = t('No answer from server');
  }
  else {

    // Extract the result into an XML response object.
    $xml = new SimpleXMLElement($result);
    $response = array();
    // Log the API response if specified.
    if ($elavon_log['response'] == 'response') {
      watchdog('uc_elavon', 'API response received:<pre>@xml</pre>', array('@xml' => $xml->asXML()));
    }

    if (isset($xml->errorCode) && isset($xml->errorName) && isset($xml->errorMessage)) {
      $response['status'] = FALSE;
      $response['msg'] = t('<b>Error Code:</b> @errorCode<br /><b>Error Name:</b> @errorName<br /><b>Error Message:</b> @errorMessage', array(
        '@errorCode' => (string) $xml->errorCode,
        '@errorName' => (string) $xml->errorName,
        '@errorMessage' => (string) $xml->errorMessage,
      ));

    }
    else {

      $response['status'] = ((string) $xml->ssl_result_message === 'APPROVAL') ? TRUE : FALSE;
      $response['ssl_txn_id'] = ($xml->ssl_txn_id) ? (string) $xml->ssl_txn_id : '';

      $payment_log_message = t('Type: @type<br />ID: @id', array('@type' => _uc_elavon_txn_type($data['txn_type']), '@id' => (string) $xml->ssl_txn_id));

      $response['payment_log_message'] = $payment_log_message;

      $msg = uc_elavon_get_log_message($xml);

      // Build an admin order comment.
      $comment = t('<b>@type</b><br /><b>@status:</b> @message<br />Amount: @amount<br />AVS response: @avs', array(
        '@type' => _uc_elavon_txn_type($data['txn_type']),
        '@status' => $response['status'] ? t('ACCEPTED') : t('REJECTED'),
        '@message' => isset($msg[0]) ? $msg[0] : '',
        '@amount' => uc_currency_format($amount),
        '@avs' => isset($msg[1]) ? $msg[1] : '',
      ));

      // Add the CVV response if enabled.
      if (variable_get('uc_credit_cvv_enabled', TRUE)) {
        $comment .= '<br />' . t('CVV match: @cvv', array('@cvv' => isset($msg[2]) ? $msg[2] : ''));
      }
      $response['msg'] = $comment;
    }

  }

  return $response;
}

/**
 * Returns the URL to the Elavon server determined by transaction mode.
 */
function uc_elavon_server_url() {
  $txn_mode = trim(variable_get('uc_elavon_txn_mode'));
  switch ($txn_mode) {
    case UC_ELAVON_TXN_MODE_PRODUCTION:
      return 'https://api.convergepay.com/VirtualMerchant/processxml.do';

    case UC_ELAVON_TXN_MODE_TEST:
      return 'https://api.demo.convergepay.com/VirtualMerchantDemo/processxml.do';
  }
}

/**
 * Build log message.
 */
function uc_elavon_get_log_message($xml) {
  $txn_mode = trim(variable_get('uc_elavon_txn_mode'));
  switch ($txn_mode) {
    case UC_ELAVON_TXN_MODE_PRODUCTION:
      $txn_mode = "LIVEMODE";
      break;

    case UC_ELAVON_TXN_MODE_TEST:
      $txn_mode = "TESTMODE";
      break;
  }

  $status = t('(@txn_mode) @message', array(
    '@txn_mode' => $txn_mode,
    '@message' => ((string) $xml->ssl_result_message) ? $xml->ssl_result_message : '',
  ));

  $avs = !empty($xml->ssl_avs_response) ? (string) $xml->ssl_avs_response : FALSE;
  $cvv = !empty($xml->ssl_cvv2_response) ? (string) $xml->ssl_cvv2_response : FALSE;
  $message = array(
    $status,
    $avs ? t('AVS response: @avs', array('@avs' => uc_elavon_avs_response($avs))) : '',
    $cvv ? t('CVV match: @cvv', array('@cvv' => uc_elavon_cvv_response($cvv))) : '',
  );

  return $message;
}

/**
 * Returns the title of the transaction type.
 */
function _uc_elavon_txn_type($type) {
  switch ($type) {
    case UC_CREDIT_AUTH_CAPTURE:
      return t('Authorization and capture');
  }
}

/**
 * Returns the message text for an AVS response code.
 */
function uc_elavon_avs_response($code) {
  switch ($code) {
    case 'A':
      return t('Address: Address matches, Zip does not');

    case 'B':
      return t('Street Match: Street addresses match for international transaction, but postal code doesn’t');

    case 'C':
      return t('Street Address: Street addresses and postal code not verified for international transaction');

    case 'D':
      return t('Match: Street addresses and postal codes match for international transaction');

    case 'E':
      return t('Error: Transaction unintelligible for AVS or edit error found in the message that prevents AVS from being performed');

    case 'G':
      return t('Unavailable: Address information not available for international transaction');

    case 'I':
      return t('Not Verified: Address Information not verified for International transaction');

    case 'M':
      return t('Match: Street addresses and postal codes match for international transaction');

    case 'N':
      return t('No: Neither address nor Zip matches');

    case 'P':
      return t('Postal Match: Postal codes match for international transaction, but street address doesn’t');

    case 'R':
      return t('Retry: System unavailable or time-out');

    case 'S':
      return t('Not Supported: Issuer doesn’t support AVS service');

    case 'U':
      return t('Unavailable: Address information not available');

    case 'W':
      return t('Whole Zip: 9-digit Zip matches, address doesn’t');

    case 'X':
      return t('Exact: Address and nine-digit Zip match');

    case 'Y':
      return t('Yes: Address and five-digit Zip match');

    case 'Z':
      return t('Whole Zip: 9-digit Zip matches, address doesn’t');

    case '0':
      return t('No response sent');

    case '5':
      return t('Invalid AVS response');
  }

  return '-';
}

/**
 * Returns the message text for a CVV match.
 */
function uc_elavon_cvv_response($code) {
  switch ($code) {
    case 'M':
      return t('CVV2/CVC2/CID Match');

    case 'N':
      return t('CVV2/CVC2/CID No Match');

    case 'P':
      return t('Not Processed');

    case 'S':
      return t('Issuer indicates that the CV data should be present on the card, but the merchant has indicated that the CV data is not present on the card.');

    case 'U':
      return t('Unknown / Issuer has not certified for CV or issuer has not provided Visa/MasterCard with the CV encryption keys.');

    case 'X':
      return t('Server Provider did not respond');

  }
  return '-';
}
