Overview
========
Elavon gateway support for Drupal Ubercart module.

Elavon Virtual Merchant (Sandbox Account)
https://api.demo.convergepay.com/VirtualMerchantDemo/

Elavon Virtual Merchant (Live Account)
https://api.convergepay.com/VirtualMerchant/

Summary
=======
Adds one payment methods:

 * Elavon direct - directly process a credit card request.

Methods
=======
Most implemented through payment forms.
